//add function
function add(a,b) {
    return a + b
}
console.log(add(2,5));

//multiplication function
function multiply(a,b) {
    let multiple = a;
    for(let i=1; i<b; i++) {
        a = add(a, multiple)
    }
    return a;
}
console.log(multiply(2,5));

//exponentiation function
function power(x,n) {
    let base = x;
    for(let i=1; i<n; i++) {
        x = multiply(x, base)
    }
    return x;
}
console.log(power(2,5));

//factorial function
function factorial(a) {
    let total = a;
    for(let i=1; i<a; i++) {
        total = multiply(total, i) 
    }
    return total;
}
console.log(factorial(5));

//fibonacci function
function fibonacci(n) {
//     let arr = [0,1,1,2];
//     for (let i=2; i<n; i++) {
//         arr.push(arr[i] + arr[i-1]);
//     }
//   return arr[arr.length - 1];

    var a = 1, b = 0, temp;

    while (n >= 0){
        temp = a;
        a = a + b;
        b = temp;
        n--;
    }

    return b;
}
console.log(fibonacci(8));